# SatNOGS AzEl-receiver-coverage plots

A simple python3 plotting script for Az/El values.
Based on <https://gitlab.com/snippets/1686218>.

![example plot](example_plot.png)

## Requirements

Install python requirements
```
pip install -r requirements.txt
```

## Usage

Open the az-el plot for station 2 in an interactive window.
```
./az_el_plot_log.py sats.csv 2
```

Generate an az-el plot for station 2 and save them in a file.
```
./az_el_plot_log.py sats.csv 2 -o az_el_plot_station2.png
```

### az\_el\_plot\_log.py - CLI options
```
usage: az_el_plot_log.py [-h] [-o FILE] FILE ID

Plot Az/El points in a polar diagram

positional arguments:
  FILE                  A .csv FILE with header, ";" delimiters and at least
                        these columns: StationID, Azimuth and Elevation
  ID                    The StationID for which the plot should be generated.

optional arguments:
  -h, --help            show this help message and exit
  -o FILE, --output FILE
                        Write the generated plot to FILE instead of showing it
                        interactively.
```

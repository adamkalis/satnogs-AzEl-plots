#!/usr/bin/env python3

from matplotlib.colors import LogNorm
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import argparse


def generate_az_el_histogram(data):
    # Binning of the data
    az_bins = np.linspace(0, 2*np.pi, 90)      # 0 to 2*pi in steps of 360/N.
    el_bins = np.linspace(0, 90, 30)
    theta, r = np.mgrid[0:2*np.pi:89j, 0:90:29j]
    H, xedges, yedges = np.histogram2d(np.radians(d.Azimuth),
                                       90 - d.Elevation,
                                       bins=(az_bins, el_bins))

    # Replace empty bins with 0 by np.nan, required e.g. for log plots
    H[H == 0] = np.nan

    return theta, r, H


def generate_az_el_plot(theta, r, H):
    # Make a square figure
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8], polar=True, facecolor='#FFFFFF')
    ax.set_theta_zero_location('N')
    ax.set_theta_direction(-1)

    # Plot
    ctf = ax.contourf(theta, r, H,
                      levels=[1e0, 1e1, 1e2, 1e3],
                      cmap=plt.cm.jet,
                      norm=LogNorm())
    plt.colorbar(ctf)

    # Customize ticks and labels
    ax.set_yticks(range(0, 90+10, 10))
    yLabel = ['90°', '', '', '60°', '', '', '30°', '', '', '0°']
    ax.set_yticklabels(yLabel)
    ax.axes.tick_params(labelsize=15)

    return ax, fig


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Plot Az/El points in ' +
                                                 'a polar diagram')
    parser.add_argument('input',
                        metavar='FILE',
                        type=argparse.FileType('r'),
                        help='A .csv FILE with header, ";" delimiters and ' +
                        'at least these columns: ' +
                        'StationID, Azimuth and Elevation')
    parser.add_argument('station_id',
                        metavar='ID',
                        type=int,
                        help='The StationID for which the plot should be ' +
                        'generated.')
    parser.add_argument('-o', '--output',
                        metavar='FILE',
                        type=argparse.FileType('w'),
                        help='Write the generated plot to FILE instead of ' +
                        'showing it interactively.')

    args = parser.parse_args()

    # Parse the data
    data = pd.read_csv(args.input, sep=";", header=0, index_col=1)

    # Select the desired station
    d = data.loc[args.station_id, ['Azimuth', 'Elevation']]

    # Generate the plot
    theta, r, H = generate_az_el_histogram(data)
    fig, ax = generate_az_el_plot(theta, r, H)

    # Save or display the plot
    if args.output:
        args.output.close()
        plt.savefig(args.output.name)
    else:
        plt.show()
